module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        less: {
            files: ['src/less/style.less', 'src/less/variables.less', 'src/less/mixins.less', 'src/less/base.less', 
            'src/less/navigation.less', 'src/less/homepage.less', 'src/less/libs/bootstrap/bootstrap.less'],
            tasks: ['less'], 
        },
        jquery: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
            tasks: ['sync:syncjquery'],
        }
    },
    less: {
        default: {
            files: [
                {
                    expand: true,
                    cwd: 'src/less',
                    src: 'style.less',
                    dest: 'public/css/',
                    ext: '.css'
                }
            ]
        }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'public/css',
          src: ['*.css', '!*.min.css'],
          dest: 'public/css',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
        my_target: {
          files: {
            'public/js/script.min.js': ['public/js/script.js']
          }
        }
      },
    sync : {
        syncybase: {
            files: [{
                cwd: 'src/js',
                src: '*.js',
                dest: 'public/js',
            },
            {
                cwd: 'src/images',
                src: '**',
                dest: 'public/images',
            }]
        },
        syncjquery: {
            files: [{
                cwd: 'src/js/libs/jquery',
                src: 'jquery*.js',
                dest: 'public/js/common',
            },{
                cwd: 'src/css/libs/jquery',
                src: 'jquery*.css',
                dest: 'public/css/common',
            }]
        },
        syncbootstrap: {
            files: [{
                cwd: 'src/js/libs/bootstrap',
                src: 'bootstrap*.js',
                dest: 'public/js/common',
            }]
        }
    },
    
});
 
  // Plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-sync');


  // Default task(s).
  grunt.registerTask('default', ['less', 'cssmin', 'sync', 'uglify']);

};
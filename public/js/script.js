var wooder = {
	/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
	openNav: function() {
	    $("#navmenu-side").css({"width": "100%", "visibility": "visible"});
	    $("#mask").addClass("active");
	},

	/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
	closeNav: function() {
	    $("#navmenu-side").css({"width": "0px", "visibility": "hidden"});
	    $("#mask").removeClass("active");
	},

	/* Call full page scroll library */
	fullpage: function() {
		$('#fullpage').fullpage({
		  sectionsColor: ['#151413', '#FFFFFF', '#FFFFFF'],
		  navigation: true,
		  navigationPosition: 'left',
		  'afterLoad': function(anchorLink, index){
				if(index == 2){
					$('#line1').addClass('active');
				}
				else if(index == 3){
					$('#line2').addClass('active');
				}
			},
			'onLeave': function(index, nextIndex, direction){
				$('.line-animate').removeClass('active');
			}
		});
	},

	/* Slide out menu */
	bindBurgerMenu: function() {
		$("#navmenu-side-button").on("click", function() {
			wooder.openNav();
		});
		$("#navmenu-side .close-button").on("click", function() {
			wooder.closeNav();
		});
	},

	/* initialise functions */
	init: function() {
		this.fullpage();
		this.bindBurgerMenu();
	}
}

$(document).ready(function() {
	wooder.init();
});